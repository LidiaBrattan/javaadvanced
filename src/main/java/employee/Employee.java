package employee;

public class Employee {

    //Am creat atributele angajatului
    private String name;
    private int age;
    private float salary;

    //constructor cu atribute "goale" pt crearea de noi angajati
    public Employee(String namePar, int agePar, float salaryPar) {
        this.name = namePar;
        this.age = agePar;
        this.salary = salaryPar;
    }

    //creare getter pentru a afla atributele fiecarui angajat creat
    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public float getSalary() {
        return salary;
    }

    //seteaza valoarea atributului angajatului
    public void setName(String name) { this.name = name; }

    public void setAge(int age) {
        this.age = age;
    }

    public void setSalary(float salary) {
        this.salary = salary;
    }

    //afiseaza la consola atributele angajatului
    public String toString(){
        return "Name: " + name + " Age " + age + " Salary " + salary;
    }
}
