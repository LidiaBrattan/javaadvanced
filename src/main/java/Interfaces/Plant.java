package Interfaces;

public abstract class Plant {

    private String name;
    private float basePrice;
    private int quantity;

    public Plant(String name, float basePrice, int quantity) {
        this.name = name;
        this.basePrice = basePrice;
        this.quantity = quantity;
    }

    public String getName() {
        return name;
    }

    public float getBasePrice() {
        return basePrice;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setBasePrice(float basePrice) {
        this.basePrice = basePrice;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public abstract float getSellingPrice();


}
