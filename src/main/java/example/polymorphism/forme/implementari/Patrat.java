package example.polymorphism.forme.implementari;

import example.polymorphism.forme.Forma2D;
import example.polymorphism.forme.trasaturi.Bordura;

public class Patrat extends Forma2D {

    private int latura;

    public Patrat(int latura){
        this.latura = latura;
    }

    public Patrat(int latura, Bordura bordura, String culoare){
        super(bordura, culoare);
        this.latura = latura;
    }

    @Override
    public double getPerimetru() {
        double perimetru = 4* latura;
        return perimetru;
    }

    public double getAria() {
        double aria = Math.pow(this.latura,2);
        return aria;
    }

    @Override
    public void afiseazaForma() {
        System.out.println("Aceasta este un patrat de culoare " + getCuloare() + " cu margine " + getBordura() +
                " cu perimetrul " + getPerimetru() + " si aria " + getAria());
    }
}
