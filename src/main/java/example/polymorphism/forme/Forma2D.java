package example.polymorphism.forme;

import example.polymorphism.forme.implementari.Cerc;
import example.polymorphism.forme.trasaturi.Bordura;

public abstract class Forma2D {
    private Bordura bordura = Bordura.SOLID;
    private String culoare;
    //facem metoda pentru a calcula perimtrul

    public Forma2D(){

    }

    public Forma2D (Bordura bordura, String culoare){
        this.bordura = bordura;
        this.culoare = culoare;
    }

    public abstract double getPerimetru();

    public abstract double getAria();

    public void afiseazaForma(){
        System.out.println("Aceasta este o forma de culoare " +this.culoare + "cu margine " + this.bordura +
                "cu perimetrul " + getPerimetru() + "si aria " + getAria());
    }

    public Bordura getBordura() {
        return bordura;
    }

    public String getCuloare() {
        return culoare;
    }


    public void setBordura(Bordura bordura) {
        this.bordura = bordura;
    }

    public void setCuloare(String culoare) {
        this.culoare = culoare;
    }
}
