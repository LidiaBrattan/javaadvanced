package animal;

public class Animal {
    //private , public, protected
    //by default is protected
    private String name;
    private String species = "abc";
    private int age;
    public static boolean visible;

    //pentru a accesa aceste variabile in alte clase facem constructorul si
    // punem public sa fie vauzut din alte pachete si clase:

    public Animal(String nameParameter, String speciesParam, int ageParam){
        this.name = nameParameter;
        this.species = speciesParam;
        this.age = ageParam;
        this.visible = visible;
    }

    public String getName() {
        return name;
    }

    public String getSpecies() {
        return species;
    }

    public int getAge() {
        return age;
    }

    public void setName(String newname) {
        this.name = newname;
    }

    public void setSpecies(String species) {
        this.species = species;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String toString(){
        return "Name: " + name + " Species: " + species + " Age: " + age;
    }
}
