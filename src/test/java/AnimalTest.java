import animal.Animal;
import animal.AnimalService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class AnimalTest {

    @Test
    public void animalCreatTestToString(){

        //given
        Animal animal = new Animal("Lidia", "vulpe", 10);

        //when
        String actualResutl = animal.toString();

        //then
        Assertions.assertEquals(actualResutl, "Name: Lidia Species: vulpe Age: 10");



    }
}
