package example.polymorphism;

import example.polymorphism.forme.Forma2D;
import example.polymorphism.forme.implementari.Cerc;

import java.util.ArrayList;
import java.util.Scanner;

public class Exercitii3 {
    public static void main(String[] args) {
        String raspuns;
        Scanner keyboard = new Scanner(System.in);
        ArrayList <Forma2D> forme = new ArrayList<Forma2D>();
         //<ce obiect vrei sa tii in aceasta lista>
        do {

            System.out.println("Alegeti forma pe care doriti sa o creati: ");
            System.out.println("1. Cerc\n" + "2. Patrat\n" + "3. Triunghi\n");
            int nrForma = keyboard.nextInt();
            keyboard.nextLine();

            ConstructorForme2D constructor = new ConstructorForme2D();
            Forma2D formaCreata = constructor.creeazaForma2D(nrForma);

            forme.add(formaCreata);

            //sus creaza un obiect de tip
            //Forma2D pentru a salva forma creata la tastatura
            formaCreata.afiseazaForma();
            System.out.println("Mai vreti sa creati o forma?" + "Da/Nu");
            raspuns = keyboard.nextLine();

        } while (raspuns.equals("Da"));

        int lungimeForme = forme.size();

        for (int i = 0; i< lungimeForme; i++){
            forme.get(0).afiseazaForma();
        }
        
    }
}
