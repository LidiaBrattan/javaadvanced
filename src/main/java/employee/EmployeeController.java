package employee;

import employee.EmployeeService;

import java.util.Scanner;

public class EmployeeController {
    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);

        EmployeeService employeeServiceConstanta = new EmployeeService("Constanta");
        EmployeeService employeeServiceSinaia = new EmployeeService("Sinaia");
        EmployeeService employeeServiceTimisoara = new EmployeeService("Timisoara");

        System.out.println("Employees from Constanta");
        employeeServiceConstanta.getUserToCreateEmployee();
        System.out.println("Employees from Sinaia");
        employeeServiceSinaia.getUserToCreateEmployee();
        System.out.println("Employees from Timisoara");
        employeeServiceTimisoara.getUserToCreateEmployee();

        employeeServiceConstanta.displayLocalEmployees();
        employeeServiceConstanta.displayAllEmployees();

        employeeServiceTimisoara.displayLocalEmployees();
        employeeServiceTimisoara.displayAllEmployees();

        System.out.println("___________________________");
        System.out.println("Select the location: ");
        System.out.println("1. Constanta");
        System.out.println("2. Sinaia");
        System.out.println("3. Timisoara");

        int optiuneOras = keyboard.nextInt();

        switch (optiuneOras) {
            case 1:
                employeeServiceConstanta.editEmployee();
                employeeServiceConstanta.displayLocalEmployees();
                break;
            case 2:
                employeeServiceSinaia.editEmployee();
                employeeServiceSinaia.displayLocalEmployees();
                break;
            case 3:
                employeeServiceTimisoara.editEmployee();
                employeeServiceTimisoara.displayLocalEmployees();
                break;
        }
        employeeServiceConstanta.displayAllEmployees();

    }

}
