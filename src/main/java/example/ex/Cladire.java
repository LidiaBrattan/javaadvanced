package example.ex;

public class Cladire extends Constructie{

    private int nrEtaje;
    private float suprafata;
    private String adresa;

    public Cladire(int nrEtaje, float suprafata, String adresa, int costProiectare, int costConstructie, TipConstructie tipConstructie){
        super(costProiectare, costConstructie, tipConstructie);
        this.nrEtaje = nrEtaje;
        this.suprafata = suprafata;
        this.adresa = adresa;
    }

    public int getNrEtaje() {
        return nrEtaje;
    }

    public float getSuprafata() {
        return suprafata;
    }

    public String getAdresa() {
        return adresa;
    }
}
