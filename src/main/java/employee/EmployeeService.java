package employee;

import animal.Animal;
import animal.AnimalService;

import java.util.Scanner;

public class EmployeeService {
    public static int contor = 0;

    public static Employee[] allEmployees = new Employee[100];

    private String location = " ";

    private Employee[] localEmployees;

    private int localcontor;

    public EmployeeService(String locationPar){
        location = locationPar;
        localEmployees = new Employee[100];
        this.localcontor = 0;
    }


    public Employee getUserToCreateEmployee() {
        Scanner keyboard = new Scanner(System.in);

        System.out.println("Please insert employee name, age and salary: ");
        String employeeName = keyboard.nextLine();
        int age = keyboard.nextInt();
        float salary = keyboard.nextFloat();

        Employee employee = createEmployee(employeeName, age, salary);
        return employee;
    }

    public Employee createEmployee (String nume, int varsta, float salariu){
        Employee employee = new Employee(nume, varsta, salariu);
        localEmployees[localcontor] = employee;
        allEmployees[contor] = employee;
        localcontor++;
        contor++;

        System.out.println("Employee: " + employee.toString());

        return employee;
    }

    public void displayAllEmployees(){
        System.out.println("All employees: ");
        for (int i = 0; i < EmployeeService.contor; i++){
            System.out.println(i + " " + allEmployees[i]);
        }
    }

    public void displayLocalEmployees(){
        System.out.println("Display local employess from: " + location);
        for (int i = 0; i < localcontor; i ++){
            System.out.println(i + " " + localEmployees[i]);
        }
    }

    public void editEmployee(){
        Scanner keyboard = new Scanner(System.in);
        System.out.println("Introduce the employee name you want to change: ");
        String nameEmployee = keyboard.nextLine();

        int positionLocalEmployee = -1; //sa nu se afle pe o pozitie existenta deja

        //finding employee in localEmployees
        for (int i = 0; i<localcontor; i++){
            Employee localEmployee = localEmployees[i];
            if (localEmployee.getName().equals(nameEmployee)){
                positionLocalEmployee = i;
            }
        }
        if (positionLocalEmployee == -1){
            System.out.println("There is no employee with the given name: " + nameEmployee + "la " + location);
            return;
        }

        Employee employeeCuDateVechi = localEmployees[positionLocalEmployee];

        int pozitieAllEmployees = -1;
        for(int i = 0; i < contor; i++){
            Employee currentEmployee = allEmployees[i];
            if (currentEmployee.getName().equals(employeeCuDateVechi.getName()) &&
                    currentEmployee.getAge() == employeeCuDateVechi.getAge() &&
                    currentEmployee.getSalary() == employeeCuDateVechi.getSalary()){
                pozitieAllEmployees = i;
            }
        }
        if (pozitieAllEmployees == -1){
            System.out.println("here is no employee with the given name: " + nameEmployee + "la " + location);
            return;
        }

        System.out.println("Write the new info: name, age and salary");
        String newName = keyboard.nextLine();
        int newAge = keyboard.nextInt();
        float newSalary = keyboard.nextFloat();

        localEmployees[positionLocalEmployee].setName(newName);
        localEmployees[positionLocalEmployee].setAge(newAge);
        localEmployees[positionLocalEmployee].setSalary(newSalary);

        allEmployees[pozitieAllEmployees].setName(newName);
        allEmployees[pozitieAllEmployees].setAge(newAge);
        allEmployees[pozitieAllEmployees].setSalary(newSalary);

    }

}


