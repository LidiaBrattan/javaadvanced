package example.polymorphism;

import example.polymorphism.forme.*;
import example.polymorphism.forme.implementari.Cerc;
import example.polymorphism.forme.implementari.Patrat;
import example.polymorphism.forme.implementari.Triunghi;
import example.polymorphism.forme.trasaturi.Bordura;

public class Exercitii {
    public static void main(String[] args) {
       Forma2D[] forme = new Forma2D[4];

       forme[0] = new Cerc(5, Bordura.SOLID, "rosu" );
       forme[1] = new Patrat(5,Bordura.DOTTED, "verde");
       forme[2] = new Triunghi(20, 10, 13, Bordura.DASHED, "albastru");
       forme[3] = new Triunghi(25, 60, Bordura.DASHED, "albastru");


       for (int i = 0; i<=3; i++){

           try {
               forme[i].afiseazaForma();
           } catch (ArithmeticException exception){
               System.out.println("Acestei forme nu ii poate fi calculat perimetrul");
               System.out.println(exception.getMessage());
           } catch (NullPointerException exception2){
               System.out.println();
               System.out.println(exception2.getMessage());
           }
       }

    }
}
