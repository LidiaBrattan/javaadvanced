package example.ex;

public class Drum extends Constructie{
    int lungime;
    int nrBenzi;

    public Drum (int costProiectare, int costConstructie, int lungime, int nrBenzi, TipConstructie tipConstructie){
        super(costProiectare, costConstructie, tipConstructie);
        this.lungime = lungime;
        this.nrBenzi = nrBenzi;
    }

    public int getLungime() {
        return lungime;
    }

    public int getNrBenzi() {
        return nrBenzi;
    }
}
