package example.polymorphism;

import example.polymorphism.forme.Forma2D;
import example.polymorphism.forme.implementari.Cerc;
import example.polymorphism.forme.implementari.Patrat;
import example.polymorphism.forme.implementari.Triunghi;
import example.polymorphism.forme.trasaturi.Bordura;

import java.util.Scanner;

public class ConstructorForme2D {
    Scanner keyboard = new Scanner(System.in);

    public Forma2D creeazaForma2D(int nrFormei){
        switch (nrFormei){
            case 1:
                return creeazaCerc();
            case 2:
                return creeazaPatrat();
            case 3:
                return creazaTriunghi();
        }
        return null;
    }

    public Cerc creeazaCerc(){
        System.out.println("Dati raza cercului: ");
        int raza = keyboard.nextInt();
        keyboard.nextLine(); //punem pt ca teoretic ar trebui sa ia si o bordura dar nu este si atunci trecem la linia urmatoare
        Cerc c = new Cerc(raza);
        seteazaTrasaturi(c);
        return c;

    }
    public Patrat creeazaPatrat(){
        System.out.println("Dati latura patratului: ");
        int latura = keyboard.nextInt();
        keyboard.nextLine();
        Patrat p = new Patrat(latura);
        seteazaTrasaturi(p);
        return p;

    }

    public Triunghi creazaTriunghi(){
        System.out.println("Dati laturile si inaltimea fata de latura: ");
        int lat1 = keyboard.nextInt();
        int lat2 = keyboard.nextInt();
        int baza = keyboard.nextInt();
        int inaltime = keyboard.nextInt();
        keyboard.nextLine();
        Triunghi t = new Triunghi(lat1, lat2, baza, inaltime);
        seteazaTrasaturi(t);
        return t;
    }

    private void seteazaTrasaturi(Forma2D forma){
        System.out.println("Dati bordura");
        String bordura = keyboard.nextLine();
        forma.setBordura(Bordura.valueOf(bordura));
        System.out.println("Dati culoarea");
        String culoare = keyboard.nextLine();
        forma.setCuloare(culoare);
        //metoda este camuflata de celelalte prin urmare nu interefereaza
    }
}
