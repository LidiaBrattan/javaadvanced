package Interfaces;

public class Mushroom extends Plant implements Edible, CanBePoisonous{

    private boolean isPoisonous;

    //pt ca am facut obiectul TRATAMENT atunci putem sa ii asignam atribute
    //aici doar il stabilim
    private PoisonMushroomTreatment treatment;

    public Mushroom(String name, float basePrice, int quantity) {
        super(name, basePrice, quantity);
        this.isPoisonous = false;
    }

    public Mushroom(String name, float basePrice, int quantity, boolean isPoisonous) {
        super(name, basePrice, quantity);
        this.isPoisonous = isPoisonous;
    }

    public Mushroom(String name, float basePrice, int quantity, boolean isPoisonous, String medicineName, TreatmentApplicationType applicationType) {
        super(name, basePrice, quantity);
        this.isPoisonous = isPoisonous;
        //aici instantiem obiectul name si ii oferim atributele name si application
        //pt a crea un obiect mushroom complet in Application
        this.treatment = new PoisonMushroomTreatment(medicineName, applicationType);
    }

    public float getSellingPrice() {
        if(treatment != null){
            return getBasePrice() * treatment.applicationType.priceFactor;
        }else {
            return getBasePrice();
        }
    }

    public String getRecepies() {
        return "Tocanita de ciuperci";
    }

    public float getCaloriesPer100g() {
        return 60;
    }

    public boolean isPoisonous() {
            return isPoisonous;

    }

    public String getTreatmeant(){
        if (treatment == null){
            return "Nu exista tratament";
        }else return "Tratamentul pentru ciuperca se numeste: " + treatment.medicineName +
                " si se aplica: " + treatment.applicationType +
                " modul: " + treatment.applicationType.applicationStrategy;
    }

    //INNER CLASS
    //facem o clasa noua care sa tina doar de tratamentul pentru ciuperci
    //ca sa nu se complice treaba sus cu atributele care tin doar de ciuperca

    private class PoisonMushroomTreatment {
        public String medicineName;
        public TreatmentApplicationType applicationType;

        public PoisonMushroomTreatment(String medicineName, TreatmentApplicationType applicationType){
            this.medicineName = medicineName;
            this.applicationType = applicationType;
        }
    }

    public enum TreatmentApplicationType {
        ORAL(2, "oral"),

        TOPIC_LOCAL (3, "pe piele"),

        TOPIC_CUTANAT (4, "in gura");

        //in enum putem sa ii mai adaugam metode sa personalizam parametrii enumului
        //cream atributele care tin doar de enum
        private float priceFactor;
        private String applicationStrategy;

        //cream constructorul care tine doar de enum
        TreatmentApplicationType (float priceFactor, String applicationStrategy){
            this.priceFactor = priceFactor;
            this.applicationStrategy = applicationStrategy;
        }

        float getPriceFactor(){
            return priceFactor;
        }

        public String getApplicationStrategy() {
            return applicationStrategy;
        }
    }
}
