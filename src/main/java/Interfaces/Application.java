package Interfaces;

import java.util.ArrayList;

public class Application {

    public static void main(String[] args) {

        exempleInterfaces();
        exampleAnnonymousClass();
        exampleInnerClass();
        exampleEqualsOverride();
    }

    public static void exampleEqualsOverride(){
        Rose one = new Rose("Trandafir", 10, 100);
        Rose theSame= new Rose("Trandafir", 10, 100);

        //trandafirii nu o sa fie aceeasi deoarece sunt 2 obiecte diferite chiar daca au atribute la fel
        if (one.equals(theSame)){
            System.out.println("Acelasi trandafir");
        }else {
            System.out.println("TRandafiri diferiti");
        }
    }

    public static void exempleInterfaces(){
        //        Plant p = new Blueberry("Afin de munte", 20f, 3);
//        Plant p2 = new Ivy("Iedera cataratoare", 30f, 2);

        //in loc sa creezi obiectul mereu, poti sa-l creezi direct in Array

        ArrayList<Plant> plants = new ArrayList<Plant>();
        plants.add(new Blueberry("Afin de munte", 10, 100));
        plants.add(new Ivy("Iedera de gradina", 3, 100));
        plants.add(new Rose("Trandafir rosu", 8, 50));
        plants.add(new Mushroom("Cipuerci Champignon", 10, 300, false));
        plants.add(new Ivy("Iedera otravitoare", 3, 10));
        plants.add(new Mushroom("Palaria sarpelui", 4, 100, true));
        plants.add(new Tulip("Lalea roz", 3, 30));

        //pentru a parcurge ArrayList, mereu facem cu .size() care ne da nr elementelor (7 in cazul de sus)
        //daca facem cu get.(2) se refera la index (se refera Rose)

        for (int i = 0; i < plants.size(); i++) {
            //ca sa nu mai repetam plants.get() facem o variabila
            Plant p = plants.get(i);

            System.out.println(plants.get(i).getName());
//            System.out.println(p.getName()); //sau puteam asa

            if ((p instanceof Edible && p instanceof CanBePoisonous && ((CanBePoisonous) p).isPoisonous() == false) ||
                    (p instanceof Edible && !(p instanceof CanBePoisonous))) {

                System.out.println("Retete: " + ((Edible) p).getRecepies());
                System.out.println("Calorii per 100 g produs neprocesat: " + ((Edible) p).getCaloriesPer100g());
            }

            System.out.println("_______________________________");
        }
    }

    public static void exampleInnerClass(){
        Mushroom otravioare = new Mushroom("ciuperca otravitoare", 10, 100,
                    true, "medicament antidot", Mushroom.TreatmentApplicationType.ORAL);

        Mushroom neOtravioare = new Mushroom("ciuperca otravitoare", 10, 100);

        System.out.println(otravioare.getTreatmeant());
        System.out.println(neOtravioare.getTreatmeant());

    }

    public static void exampleAnnonymousClass(){
        Plant cactus = new Plant("cactus bunny", 10, 100) {
            @Override
            public float getSellingPrice() {
                return (getBasePrice() * 1.5f);
            }
        };// pui ; pentru ca practic creezi si faci metoda in acelasi timp

        //Facem un obiect care sa mosteneasca metoda din Edible
        //facem new Rose pentru ca stim ca e din aceeasi familie deci vrem sa ii mosteneasca si atributele
        Edible trandafir = new Rose("Trandafir", 20, 100);


        //nu am mai folosit obiectul piersica dar ne intereseaza sa ii apelam metodele din Edible
        //este new Edible pentru ca nu avem o planta cu caracteristicile unei piersici dar vrem sa ii facem reteta si caloriile
        //oricum..asa sa fie:))
        Edible faina = new Edible() {
            public void displayBrand() {
                System.out.println("Faina Boromir");
            }

            public String getRecepies() {
                displayBrand();
                return "Paine, cozonac";
            }

            public float getCaloriesPer100g() {
                return 500;
            }
        };
//        faina.getRecepies() //avand in vedere ca e clasa anonima, putem apela doar metode din Edible
        //nu si din clasa anonima creata adica ce tine de faina
        //deci nu putem apela faina.displayBrand();
    }
}
