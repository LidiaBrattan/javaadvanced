package Interfaces;

public interface CanBePoisonous {

    boolean isPoisonous();

}
