package Interfaces;

public class Tulip extends Plant{

    public Tulip(String name, float basePrice, int quantity) {
        super(name, basePrice, quantity);
    }

    public float getSellingPrice() {
        return 0;
    }
}
