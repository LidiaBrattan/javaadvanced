package collections;

import Interfaces.*;

import java.util.*;

public class Application {

    public static void main(String[] args) {

        listExemples();
        setExemples();
        mapExamples();
        
        //daca vrem sa apelam metoda dar nu vrem neaparat sa ii dam parametrii putem apela pachetul Collections si
        //sa ii dam emptylist sau un singur element
        findGreatest(Collections.EMPTY_LIST, null);
        findGreatest(Collections.singletonList("Ana"), null);
    }

    public static void findGreatest(List list, Comparator comparator){

    }

    public static void listExemples(){
        //exista mai multe moduri de a retine obiecte, nu doar array si ArrayLista

        //ARRAYLIST
        List<Element> elements = new ArrayList<Element>();
        elements.add(new Element(14));
        elements.add(new Element(15));
        elements.add(new Element(16));
        elements.add(new Element(17));

        //adaugi un element specific pe o pozitie specifica
        elements.add(4, new Element(13));

        //mai adaugi odata obiectele create in elements
        elements.addAll(elements);

        //pt a-l parcurge
        for (int i = 0; i<elements.size(); i ++){
            System.out.println(elements.get(i).numar);
        }

        //sau

        for (Element e : elements){
            System.out.println(e.numar);
        }

        //LINKEDLIST
        List<Integer> listIntegers = new LinkedList<Integer>();
        listIntegers.add(1);
        listIntegers.add(2);
        listIntegers.add(3);
        listIntegers.add(4);
        listIntegers.add(3);
        listIntegers.add(2);
        listIntegers.add(1);

        listIntegers.add(0,10);

        for(Integer i : listIntegers) {
            System.out.println(i);
        }
    }

    public static void setExemples(){
        System.out.println("HashSet- ordinea nu este nici logica, nici cea in care au fost inserate ci aleatorie");
        //SETURI
        //HashSet
        Set<Integer> hashSetInteger = new HashSet<Integer>();
        hashSetInteger.add(0);
        hashSetInteger.add(1);
        hashSetInteger.add(2);
        hashSetInteger.add(3);
        hashSetInteger.add(3);
        hashSetInteger.add(0);
        hashSetInteger.add(11);
        hashSetInteger.add(6);

        //Daca se pun elemente care se repeta, adica au aceeasi valoare, nu le proceseaza, le ignora
        for(Integer i : hashSetInteger){
            System.out.println(i);
        }
        //vedem ca in HashSet ordinea este aleatoare, nu le pune nici in ordinea in care le-ai pus tu
        //nici intr-o ordine logica

        System.out.println("Treeset - ordinea logica, nu cea in care au fost inserate.");
        //TREESET
        Set<String> treeSetStringuri = new TreeSet<String>();
        treeSetStringuri.add("Brianna");
        treeSetStringuri.add("Carol");
        treeSetStringuri.add("Iulia");
        treeSetStringuri.add("Andreea");
        treeSetStringuri.add("Elena");
        //treeset le pune in ordinea logica, nu cum le punem noi
        for (String s : treeSetStringuri){
            System.out.println(s);
        }

        System.out.println("LinkedHashSet - ordinea in care au fost inserate.");
        //LINKEHASHSET
        Set<String> linkedHashSet = new LinkedHashSet<String>();
        linkedHashSet.add("Mirel");
        linkedHashSet.add("Cornel");
        linkedHashSet.add("Viorel");

        for (String s : linkedHashSet){
            System.out.println(s);

        }
    }

    public static void mapExamples(){

        //se creaza un loc in memorie unde pe primul loc e cheia adica pozitia si al doilea e atributul
        //primul integer este sa definesti locul adica cheia printr- un nr  dupa care sa gasesti campionul
        //String pt a pune numele campionului
        Map<Integer, String> champions = new HashMap<Integer, String>();
        champions.put(1, "Ana");
        champions.put(2, "Mirel");
        champions.put(3, "Maria");
        champions.put(4, "Alexandra");
        champions.put(5, "Bobita");
        System.out.println(champions.get(3));

        for(String nume : champions.values()){
            System.out.println(nume);
        }
        //daca vrei sa pui altceva pe pozitia 5, iti va sterge Bobita si va pune acel ceva in locul lui

        Comparator<String> ordineInversa = Comparator.reverseOrder();
        Comparator<String> ordineaMea = new Comparator<String>() {
            //daca o1 < o2 => ceva negativ
            //daca o1 == o1 => 0
            //daca o1 > o2 => ceva pozitiv
            @Override
            public int compare(String o1, String o2) {
                if(o1.length()< o2.length()){
                    return -1;
                }else if (o1.length() == o2.length()){
                    return 0;
                }else
                return 1;
            }
        };



        //TREEMAP
        //daca faci un treeMap o sa iti puna cheile conform Stringului deci in ordine alfabetica
        Map<String, Plant> treeMapPlants = new TreeMap<String, Plant>();
        treeMapPlants.put("trandafir", new Rose("Trandafir rosu", 10,100));
        treeMapPlants.put("trandafir", new Blueberry("Afin de ghiveci", 10,100));
        treeMapPlants.put("trandafir", new Ivy("Iedera de copac", 10,100));
        treeMapPlants.put("trandafir", new Mushroom("Palaria sarpelui", 10,100));

        for (Plant p : treeMapPlants.values()){
            System.out.println(p);
        }

        //LINKEDHASHMAP
        //pastreaza ordinea de inserare
        Map<String, Plant> LinkedHashMapPlants = new LinkedHashMap<String, Plant>();
        LinkedHashMapPlants.put("trandafir", new Rose("Trandafir rosu", 10,100));
        LinkedHashMapPlants.put("trandafir", new Blueberry("Afin de ghiveci", 10,100));
        LinkedHashMapPlants.put("trandafir", new Ivy("Iedera de copac", 10,100));
        LinkedHashMapPlants.put("trandafir", new Mushroom("Palaria sarpelui", 10,100));

        for (Plant p : LinkedHashMapPlants.values()){
            System.out.println(p);
        }

        //HASHMAP
        //ti le va pune aleatoriu
        Map<String, Plant> plante = new HashMap<String, Plant>();
        plante.put("trandafir", new Rose("Trandafir rosu", 10,100));
        plante.put("trandafir", new Blueberry("Afin de ghiveci", 10,100));
        plante.put("trandafir", new Ivy("Iedera de copac", 10,100));
        plante.put("trandafir", new Mushroom("Palaria sarpelui", 10,100));

        for (Plant p : plante.values()){
            System.out.println(p);
        }
    }
}
