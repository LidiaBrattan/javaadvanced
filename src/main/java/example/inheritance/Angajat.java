package example.inheritance;

public class Angajat {

    String nume;
    String functie;
    int varsta;
    float salariul;

    public Angajat (String nume, String functie, int varsta, float salariul){
        this.nume = nume;
        this.functie = functie;
        this.varsta = varsta;
        this.salariul = salariul;
    }
}
