package animal;

import java.util.Scanner;

public class AnimalService {
    //atribute statice ce tin de clasa
    public static int contor = 0;
    public static Animal[] animals = new Animal[100];
    //atribute non-statice ce tin de obiect
    private String location = " ";
    private Animal[] localAnimals;

    private int localContor;

    //facem constructor
    public AnimalService(String locationParam) {
        location = locationParam;
        localAnimals = new Animal[100];
        this.localContor = 0;
    }

    public String getLocation() {
        return location;
    }

    //daca nu faci metodele statice, atunci trebuie sa creezi un animal de timpp
//AnimalService ca sa poti sa apelezi metodele non statice

    public Animal getUserToCreateAnimal() {
        Scanner keyboard = new Scanner(System.in);

        System.out.println("Please insert animal name, species and age: ");
        String animalName = keyboard.nextLine();
        String species = keyboard.nextLine();
        int age = keyboard.nextInt();

        Animal animal = createAnimal(animalName, species, age);

        return animal;
    }

    public Animal createAnimal(String nume, String specie, int varsta) {
        Animal animal = new Animal(nume, specie, varsta);

        //add animal to local storage
        //sirul in care vrem sa stocam[pozitia la care vrem sa stocam] = ce vrem sa stocam acolo
        localAnimals[localContor] = animal;
        localContor++;

        //add to global animal storage
        animals[contor] = animal;
        contor++;


        System.out.println("Saved animal: " + animal.toString());

        return animal;
    }


    public void displayAllAnimals() {
        System.out.println("Toate animalele: ");
        for (int i = 0; i < AnimalService.contor; i++) {
            System.out.println(i + " " + animals[i]);
        }
    }

    public void displayLocalAnimals() {
        System.out.println("Display local animals from: " + location);
        for (int i = 0; i < localContor; i++) {
            System.out.println(i + " " + localAnimals[i]);
        }
    }

    public void editAnimal() {
        // presupunem ca numele animalelor e unic in acelasi oras
        System.out.println("Dati numele animalului");
        Scanner keyboard = new Scanner(System.in);
        String numeAnimal = keyboard.nextLine();


        int pozizitieLocalAnimal = -1;

        // găsim animalul în localAnimals
        for (int i = 0; i < localContor; i++) {
            Animal localAnimal = localAnimals[i];
            if (localAnimal.getName().equals(numeAnimal)) {
                pozizitieLocalAnimal = i;
            }
        }

        if (pozizitieLocalAnimal == -1) {
            System.out.println("Nu există niciun animal pe nume " + numeAnimal + " la " + location);
            return;
        }

        Animal animalCuDateVechi = localAnimals[pozizitieLocalAnimal];

        int pozitieStaticAnimals = -1;
        // gasim animalul in animals (cele static)
        // presupunem ca
        for (int i = 0; i < contor; i++) {
            Animal currentAnimal = animals[i];
            if (currentAnimal.getName().equals(animalCuDateVechi.getName()) &&
                    currentAnimal.getSpecies().equals(animalCuDateVechi.getSpecies()) &&
                    currentAnimal.getAge() == animalCuDateVechi.getAge()) {
                pozitieStaticAnimals = i;
            }
        }

        if (pozitieStaticAnimals == -1) {
            System.out.println("Nju există niciun animal pe numele " + numeAnimal + " global");
            return;
        }

        System.out.println("Scrieți noile date: nume, specie și vârstă pentru " + animalCuDateVechi.toString());
        String noulNume = keyboard.nextLine();
        String nouaSpecie = keyboard.nextLine();
        int nouaVârstă = keyboard.nextInt();


        localAnimals[pozizitieLocalAnimal].setName(noulNume);
        localAnimals[pozizitieLocalAnimal].setSpecies(nouaSpecie);
        localAnimals[pozizitieLocalAnimal].setAge(nouaVârstă);


        animals[pozitieStaticAnimals].setName(noulNume);
        animals[pozitieStaticAnimals].setSpecies(nouaSpecie);
        animals[pozitieStaticAnimals].setAge(nouaVârstă);
    }

}

