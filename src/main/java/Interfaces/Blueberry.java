package Interfaces;

public class Blueberry extends Plant implements Edible{

    public Blueberry(String name, float basePrice, int quantity) {
        super(name, basePrice, quantity);
    }

    public float getSellingPrice() {
        return 0;
    }

    public String getRecepies() {
        return "Placinta cu afine";
    }

    public float getCaloriesPer100g() {
        return 0;
    }
}
