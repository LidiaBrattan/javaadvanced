import org.junit.jupiter.api.DynamicTest;
import animal.Animal;
import animal.AnimalService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.platform.commons.util.StringUtils;

import java.util.ArrayList;
import java.util.Arrays;

    public class AnimalServicesTest {

    AnimalService service = new AnimalService("Test");

    @Test
    public void animalCreateTest() {
        // Given
        String animalName = "Geroge";
        String animalSpecies = "Tigru";
        int animalAge = 3;

        // When
        Animal animal = service.createAnimal(animalName, animalSpecies, animalAge);

        // Then
        Assertions.assertEquals(animalName, animal.getName());
        Assertions.assertEquals(animalSpecies, animal.getSpecies());
        Assertions.assertEquals(animalAge, animal.getAge());

        Assertions.assertEquals(1, AnimalService.contor);
        Assertions.assertEquals(animalName, AnimalService.animals[0].getName());
        Assertions.assertEquals(animalSpecies, AnimalService.animals[0].getSpecies());
        Assertions.assertEquals(animalAge, AnimalService.animals[0].getAge());

        System.out.println(Arrays.deepToString(AnimalService.animals));
    }


}

