package example.polymorphism.forme.trasaturi;

public enum Bordura {
    SOLID, DASHED, DOTTED
}
