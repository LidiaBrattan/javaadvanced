package example.polymorphism.forme.implementari;

import example.polymorphism.forme.Forma2D;
import example.polymorphism.forme.trasaturi.Bordura;

public class Triunghi extends Forma2D {

    private int baza;
    private int inaltime;
    private int lat2;
    private int lat3;

    public Triunghi(int lat2, int lat3, int baza, int inaltime){
        this.baza = baza;
        this.lat2 = lat2;
        this.lat3 = lat3;
        this.inaltime = inaltime;
    }
    public Triunghi(int baza, int inaltime, Bordura bordura, String culoare) {
        super(bordura, culoare);
        this.baza = baza;
        this.inaltime = inaltime;

    }

    public Triunghi(int baza, int lat2, int lat3, Bordura bordura, String culoare) {
        super(bordura, culoare);
        this.lat2 = lat2;
        this.lat3 = lat3;
        this.baza = baza;
    }

    @Override
    public double getPerimetru() {
        if (lat2 == 0 || lat3 == 0 || baza == 0) {
            ArithmeticException exception = new ArithmeticException("Triunghuil nu are toate laturile initializate ");
            throw exception;
            //se putea scri si asa:
            //throw new ArithmeticException
        } else {
            double perimetru = lat2 + lat3 + baza;
            return perimetru;
        }
    }

    public double getAria() {
        if (baza == 0 || inaltime == 0) {
            ArithmeticException exception = new ArithmeticException("Nu se poate calcula aria");
            throw exception;
        } else {
            double aria = (baza * inaltime) / 2;
            return aria;
        }
    }

    public void descrieTriunghi() {
        System.out.println("Avem un triunghi cu laturile: " + lat2 + baza);
    }

    public void descriereTiunghi(boolean prinLaturi) {
        System.out.println("Avem un triunghi cu laturile: " + lat2 + baza);


    }

    @Override
    public void afiseazaForma() {
        try {
            System.out.println("Aceasta este un triunghi de culoare " + getCuloare() + " cu margine " + getBordura() +
                    " cu perimetrul " + getPerimetru() + " si aria " + getAria());
            return;
        } catch (ArithmeticException e) {

        }

        try {
            System.out.println("Aceasta este un triunghi de culoare " + getCuloare() + " cu margine " + getBordura() +
                    " cu perimetrul " + getPerimetru());
            return;
        } catch (ArithmeticException e) {

        }

        try {
            System.out.println("Aceasta este un triunghi " +
                    " de culoare " + getCuloare() +
                    " cu margine " + getBordura() +
                    " si aria " + getAria());
            return;
        } catch (ArithmeticException e) {
            System.out.println("Nici perimetru nici aria ...");
        }
    }
}
