package example.ex;

public class Hala extends Cladire {

    String utilizare;

    public Hala (int nrEtaje, String utilizare, float suprafata, String adresa, int costProiectare, int costConstructie, TipConstructie tipConstructie) {
        super(nrEtaje, suprafata, adresa, costConstructie,costProiectare, tipConstructie);
        this.utilizare = utilizare;
    }

    public String getUtilizare() {
        return utilizare;
    }
}
