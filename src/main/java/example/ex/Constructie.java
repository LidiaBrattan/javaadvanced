package example.ex;

public class Constructie {

    private int costProiectare;
    private int costConstructie;
    private TipConstructie tipConstructie;

    public Constructie (int costProiectare, int costConstructie, TipConstructie tipConstructie){
        this.costProiectare = costProiectare;
        this.costConstructie = costConstructie;
        this.tipConstructie = tipConstructie;
    }

    public int getCostProiectare() {
        return costProiectare;
    }

    public int getCostConstructie() {
        return costConstructie;
    }
}
