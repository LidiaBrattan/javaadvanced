package Interfaces;

public class Rose extends Plant implements Edible{

    public Rose(String name, float basePrice, int quantity) {
        super(name, basePrice, quantity);
    }

    public float getSellingPrice() {
        return 0;
    }

    public String getRecepies() {
        return "Dulceata de trandafiri";
    }

    public float getCaloriesPer100g() {
        return 0;
    }

    //facem override la equals pentru a gasi asemanarea dintre Rose dupa atribute si nu dupa locul lui in memorie
    //one nu are acelasi loc in memorie cu theSame dar au aceleasi atribute si pentru a ne da asemanarile cu .equals
    //trebuie sa ii facem override astfel:
    @Override
    public boolean equals(Object obj){
        //verificam daca obiectul (care se refera la obiecte viitoare de tip rose) este o instanta a lui Rose
        if (obj instanceof Rose){
            Rose rose = (Rose) obj; //am creat acest obiect pt a fi mai usor la castare
            if (rose.getName().equals(this.getName()) &&
                    rose.getBasePrice() == this.getBasePrice() &&
                    rose.getQuantity() == this.getQuantity());
            return true;
        }
        return false;
    }
}
