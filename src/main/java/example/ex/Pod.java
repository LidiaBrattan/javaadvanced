package example.ex;

public class Pod extends Constructie{
    private int inaltime;
    private int lungime;
    private TipPod tipPod;

    public Pod (int costProiectare, int costConstructie, int inaltime, int lungime, TipPod tipPod, TipConstructie tipConstructie){
        super(costProiectare, costConstructie, tipConstructie);
        this.inaltime = inaltime;
        this.lungime = lungime;
        this.tipPod = tipPod;
    }

    public int getInaltime() {
        return inaltime;
    }

    public int getLungime() {
        return lungime;
    }
}
