package example.ex;

public class Bloc extends Cladire {

    private int nrApartamente;

    public Bloc (int nrEtaje, int nrApartamente, float suprafata, String adresa,int costProiectare, int costConstructie){
        super(nrEtaje,suprafata,adresa, costConstructie,costProiectare, TipConstructie.PRIVAT);
        this.nrApartamente = nrApartamente;
    }

    public int getNrApartamente() {
        return nrApartamente;
    }
}
