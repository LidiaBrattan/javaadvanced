package example.inheritance;
//clasa copil extends clasa parinte
public class Elev extends Persoana{


    float media;

    public Elev (String nume, int varsta, float medie){
        this.nume = nume;
        this.varsta = varsta;
        this.media = medie;
    }


    public String getNume() {
        return nume;
    }
}
