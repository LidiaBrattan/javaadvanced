package example.ex;

public class Casa extends Cladire {

    private int nrCamere;
    private TipCasa tipCasa;

    public Casa (int nrEtaje, float suprafata, int nrCamere, String adresa, int costProiectare, int costConstructie, TipCasa tipCasa, TipConstructie tipConstructie){
        super(nrEtaje, suprafata,adresa, costConstructie, costProiectare, tipConstructie);
        this.nrCamere = nrCamere;
        this.tipCasa = tipCasa;
    }

    public int getNrCamere() {
        return nrCamere;
    }
}
