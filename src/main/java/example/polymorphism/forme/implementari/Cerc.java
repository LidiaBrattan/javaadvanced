package example.polymorphism.forme.implementari;

import example.polymorphism.forme.Forma2D;
import example.polymorphism.forme.trasaturi.Bordura;

public class Cerc extends Forma2D {

    private int raza;

    public Cerc(int raza){
        this.raza = raza;
    }

    public Cerc (int raza, Bordura bordura, String culoare){
        super(bordura, culoare);
        this.raza = raza;
    }
    //aceasta metoda suprascrie o metoda din clasa parinte (ovverride)
    @Override
    public double getPerimetru() {
        double perimetru = 2*3.14 * raza;
        return perimetru;
    }


    public double getAria() {
        double aria = Math.PI*this.raza*this.raza;
        return aria;
    }

    @Override
    public void afiseazaForma() {
        System.out.println("Aceasta este un cerc de culoare " + getCuloare() + " cu margine " + getBordura() +
                " cu perimetrul " + getPerimetru() + " si aria " + getAria());
    }

}
