package Interfaces;

public interface Edible {
    //interfetele au metode care ogliga o clasa sa o implementeze
    //daca implementam aceasta interfata acelei clase

    String getRecepies();

    float getCaloriesPer100g();
}
