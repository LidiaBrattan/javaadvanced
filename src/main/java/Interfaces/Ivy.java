package Interfaces;

public class Ivy extends Plant implements CanBePoisonous {

    public Ivy(String name, float basePrice, int quantity) {
        super(name, basePrice, quantity);
    }

    public float getSellingPrice() {
        return 0;
    }

    public boolean isPoisonous() {
        return true;
    }
}
