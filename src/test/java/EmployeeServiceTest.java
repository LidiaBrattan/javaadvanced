import employee.Employee;
import employee.EmployeeService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

public class EmployeeServiceTest {
    EmployeeService service = new EmployeeService("Test");

    @Test
    public void employeeCreateTest(){
        //Given
        String employeeName ="Lidia";
        int employeeAge = 22;
        float employeeSalary = 3000;

        //when
        Employee employee = service.createEmployee(employeeName, employeeAge, employeeSalary);

        //then
        Assertions.assertEquals(employeeName,employee.getName());
        Assertions.assertEquals(employeeAge,employee.getAge());
        Assertions.assertEquals(employeeSalary,employee.getSalary());

        Assertions.assertEquals(1, EmployeeService.contor);
        Assertions.assertEquals(employeeName, EmployeeService.allEmployees[0].getName());
        Assertions.assertEquals(employeeAge, EmployeeService.allEmployees[0].getAge());
        Assertions.assertEquals(employeeSalary, EmployeeService.allEmployees[0].getSalary());

        System.out.println(Arrays.deepToString(EmployeeService.allEmployees).indexOf(0));
    }
}
