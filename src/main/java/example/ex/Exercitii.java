package example.ex;

import example.inheritance.Angajat;
import example.inheritance.Elev;

public class Exercitii {
    public static void main(String[] args) {

        Casa foster = new Casa(1, 154f, 2, "Magurelelor", 12000, 100000, TipCasa.SIMPLA, TipConstructie.PUBLIC);
        Cladire cladire = new Cladire(5, 1452f, "Lalelelor", 3666, 60000, TipConstructie.PUBLIC);
        Drum drum = new Drum(3000, 80000, 120000, 3, TipConstructie.PUBLIC);
        Hala hala = new Hala(1, "Echipament agricol", 300, "Gataia", 6000, 90000, TipConstructie.PUBLIC);
        Pod pod = new Pod(3000, 900000, 300, 3000, TipPod.SUSPENDAT, TipConstructie.PUBLIC);
        Bloc bloc = new Bloc(10, 300, 600, "Republicii", 6000, 90000);

        Constructie[] sirConstructii = new Constructie[100];
        sirConstructii[0] = foster;
        sirConstructii[1] = cladire;
        sirConstructii[2] = drum;
        sirConstructii[3] = hala;
        sirConstructii[4] = pod;
        sirConstructii[5] = bloc;

        int sumConstr = 0;
        int sumProiect = 0;

        for (int i = 0; i < sirConstructii.length; i++) {
            if (sirConstructii[i] != null) {
                sumConstr = sumConstr + sirConstructii[i].getCostConstructie();
                sumProiect = sumProiect + sirConstructii[i].getCostProiectare();
            }
        }

        System.out.println("Suma costurilor de constructie este: " + sumConstr);
        System.out.println("SUma costurilor de proiect este: " + sumProiect);

        int maxLocal = sirConstructii[0].getCostConstructie();

        for(int i = 0; i<=4; i++){
            if(sirConstructii[i].getCostConstructie()>maxLocal){
                maxLocal = sirConstructii[i].getCostConstructie();
            }
        }
        System.out.println("Costul cel mai mare de constructie este: " + maxLocal);

        //daca costul de proiectare este mai mare cu 50% decat costul de constructie
        //atunci cladirea aia e ilegala
        //afiseaza cate cladiri ilegale exista


        int contorCladiriIlegale = 0;
        for (int i = 0; i<=4; i++){
            if (sirConstructii[i].getCostProiectare()>0.5*sirConstructii[i].getCostConstructie()){
                System.out.println(sirConstructii[i].getClass());
                contorCladiriIlegale++;
            }
        }
        System.out.println("Nr cladiri ilegale = " +contorCladiriIlegale);
    }
}
