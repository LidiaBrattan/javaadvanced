package example.polymorphism;

import example.polymorphism.forme.trasaturi.Bordura;
import example.polymorphism.forme.implementari.Cerc;
import example.polymorphism.forme.implementari.Patrat;
import example.polymorphism.forme.implementari.Triunghi;

import java.util.Scanner;

public class Exercitii2 {
    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);
        //intreaba utilizator ce forma vrea sa introduca
        System.out.println("Ce forma doriti sa introduceti? Selectati un numar");
        System.out.println("1. Patrat, 2. Cerc, 3. Triunghi");
        int nrForma = keyboard.nextInt();
        //1. patrat, 2. cerc, 3 patrat
        switch (nrForma){
            case 1:
                System.out.println("Introduceti dimensiunile latului: ");
                int latura = keyboard.nextInt();
                Patrat p = new Patrat(latura, Bordura.SOLID, "alb");
                System.out.println("Perimetrul patratului este: " + p.getPerimetru());
                System.out.println("Aria patratului este: " + p.getAria());
                break;
            case 2:
                System.out.println("Introduceti dimensiunea razei: ");
                int raza = keyboard.nextInt();
                Cerc c = new Cerc(raza, Bordura.SOLID, "roz");
                System.out.println("Perimetrul cercului este: " + c.getPerimetru());
                System.out.println("Aria cercului este: " + c.getAria());
                break;
            case 3:
                System.out.println("Introduceti urmatoarele dim pt triunghi: ");
                System.out.println("3 laturi: ");
                int lat1 = keyboard.nextInt();
                int lat2 = keyboard.nextInt();
                int lat3 = keyboard.nextInt();
                System.out.println("Si inaltimea: ");
                int inaltime = keyboard.nextInt();
                Triunghi t = new Triunghi(lat3, inaltime,Bordura.SOLID, "rosu");
                Triunghi t2 = new Triunghi(lat3, lat2, lat1, Bordura.SOLID, "galben");
                System.out.println("Perimetrul triunghiului este: " + t.getAria());
                System.out.println("Aria triunghiului este: " + t2.getPerimetru());
                break;
        }


    }
}
